#include <stdio.h>
#include <stdlib.h>
#include <time.h> 

#include "FuncionesT1.h"

int *vectorGenerado ; 
int *vectorGeneradoCopia ; 
int suma , prom ; 
float mediana ;  
 
int main (){
   srand(time(NULL)) ;  /* para no generar siempre la misma secuencia e rand() */
   int longitud ; 
     
   longitud = GenerarVector() ;
   InvertirVectorMayoraMenor(longitud) ; 
   InvertirVectorMenoraMayor(longitud) ; 
   CalcularPMS(longitud) ;
   ObtenerValorPosMayor(longitud) ; 
   ObtenerValorPosMenor(longitud) ;  
   return 0 ;  
}