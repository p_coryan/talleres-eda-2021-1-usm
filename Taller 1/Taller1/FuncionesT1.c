#include <stdlib.h>
#include <stdio.h>
/* genera un vector de tamano aleatorio 
de numeros enteros y aleatorios y se muestra en pantalla */
extern int *vectorGenerado ; 
extern int *vectorGeneradoCopia ; 
extern int suma , prom ;  
extern float mediana  ;

int GenerarVector ( ){
    int longitud =  rand()%100 ;  
    vectorGenerado = (int *)malloc (longitud*sizeof(int));
    vectorGeneradoCopia = (int *)malloc (longitud*sizeof(int));
    printf("la longitud del arrays es : %d \n", longitud);
    printf("el vector aleatorio es : \n") ; 
    int i ; 
    for (i=0 ; i<longitud ; i++){
         vectorGenerado[i]= rand()%100 ;
         vectorGeneradoCopia[i] = vectorGenerado[i] ;
         printf("posicion %d es : %d \n" , i , vectorGenerado[i] ) ; 
    }
   
    return longitud  ; 
}

/* funcion para ordenar vector de mayor a menor y viceversa */
void InvertirVectorMayoraMenor(  int longitud){
     int i , j , aux  ;   
     for (i=0 ; i<longitud ; i++){
            for (j=0 ; j<longitud-i-1; j++){
                    if (vectorGenerado[j]<vectorGenerado[j+1]){
                        aux= vectorGenerado[j+1] ; 
                        vectorGenerado[j+1] = vectorGenerado[j] ; 
                        vectorGenerado[j] = aux ;     
                    }
            }
            
        } 
    printf("vector ordeando de mayor a menor \n ") ;  
    int k ; 
    for (k=0 ; k<longitud ; k++){
        printf("salida posicion %d :  %d \n",k , vectorGenerado[k]  ) ; 
        
    }

}

void InvertirVectorMenoraMayor( int longitud){
     int i , j , aux  ;   
     for (i=0 ; i<longitud ; i++){
            for (j=0 ; j<longitud-i-1; j++){
                    if (vectorGenerado[j]>vectorGenerado[j+1]){
                        aux= vectorGenerado[j+1] ; 
                        vectorGenerado[j+1] = vectorGenerado[j] ; 
                        vectorGenerado[j] = aux ;     
                    }
            }
            
        } 
    
    int k ; 
    printf("vector ordenado de menor a mayor : \n") ;
    for (k=0 ; k<longitud ; k++){
        printf("salida posicion %d :  %d \n",k , vectorGenerado[k]  ) ; 
        
    }

}

void CalcularPMS ( int longitud ){
   int i , suma = 0 , prom=0 ; 
   for( i = 0 ; i <longitud ;  i++){
       suma = suma + vectorGenerado[i] ; 
   }
   prom =  suma/longitud ;  
   if ( longitud%2 == 0 ){  /* por si hay 2 medianas */
        int LonMed1 = (longitud/2 -1); 
        int LonMed2 = (longitud/2 );
        mediana = ((float)vectorGenerado[LonMed1] + (float)vectorGenerado[LonMed2])/2 ;
   }else{
        mediana = vectorGenerado[(int)((longitud/2))] ;
   }

   printf("el promedio es: %d \n La mediana es : %.2f \n La suma es : %d \n", prom , mediana, suma) ; 

}

void ObtenerValorPosMenor (int longitud){
    int valorMin = 101 ;  
    int i , pos  ;
    for(i=0 ; i<longitud ; i++){
        if(vectorGeneradoCopia[i] < valorMin){
            valorMin = vectorGeneradoCopia[i] ; 
            pos =  i  ; 
        }
    } 
    printf("el valor min es : %d en la poscion %d \n", valorMin , pos) ; 
}

void ObtenerValorPosMayor (int longitud){
    int valorMax = -1 ;  
    int i , pos  ;
    for(i=0 ; i<longitud ; i++){
        if(vectorGeneradoCopia[i] > valorMax){
            valorMax = vectorGeneradoCopia[i] ; 
            pos =  i  ; 
        }
    } 
    printf("el valor maximo es : %d en la poscion %d \n", valorMax , pos) ; 
}
