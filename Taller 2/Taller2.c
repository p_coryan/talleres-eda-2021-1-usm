#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "FuncionesT2.h"



int main () {

srand(time(NULL)) ; 
/*------------------------pregunta 3----------------------------------------*/
int a = 20 ; 
int b = 3 ; 
int Mayor ; 
NumeroMayor(&a , &b ,&Mayor ) ;  // con punteros 
printf("el numero mayor es %d \n", Mayor) ;  // para este caso Mayor debe valer 20  
a = 2 ; 
b = 10 ; 
int *punA = &a ;
int *punB = &b ; 
int *punMayor = &Mayor ; 
NumeroMayor(punA , punB , punMayor) ; // con referencia  
printf("el numero mayor es %d \n", *punMayor) ; 

/*------------------------pregunta 4----------------------------------------*/

int * arraysN ; 
int nElementos ; 

printf("ingresar el numero de elemntos del arrays:\n" ) ;
scanf("%d", &nElementos) ;

arraysN = (int*)malloc(nElementos*sizeof(int))  ;
generarElementos(arraysN , &nElementos) ;

printf("los elementos del arrays son \n") ; 
for (int i=0 ; i<nElementos ; i++){
    printf("%d \n", arraysN[i] );
}

FILE * fileOut ; 
fileOut = fopen("out.txt" , "w") ;
for (int i=0 ; i<nElementos ; i++){
    fprintf(fileOut, "%d\n",arraysN[i]) ;
}
fclose(fileOut) ;
free(arraysN) ;
/*------------------------pregunta 5----------------------------------------*/

char * cadenaCaracter ; 
int nCaracteres = 4 ; 
cadenaCaracter= (char*)malloc((nCaracteres+1)*sizeof(char)) ; // mas 1 por caracter \0 que siempre se pone al final 
strcpy(cadenaCaracter, "hola") ;
permutarCaracter(cadenaCaracter , &nCaracteres) ;
free(cadenaCaracter) ;

/*------------------pregunta 6----------------------------------------------*/
char * cadenaString ;  
cadenaString = (char*)malloc(8*sizeof(char)) ;
strcpy(cadenaString , "hola") ; 
int largoCadenaString  =  lengthString(cadenaString) ; 
free(cadenaString) ;

/*----------------pregunta 7 ---------------------------------------------*/
int numero1 = 2 ;
int numero2 = 5 ;

swap ( &numero1 , &numero2) ; 
printf("el valor del elemento 1 es : %d  \n el valor del elemento 2 es : %d \n", numero1 , numero2) ; 



/*----------------pregunta 8 ---------------------------------------------------------*/

char * vueltaStrings ;
vueltaStrings = (char*)malloc(14*sizeof(char)) ; 
strcpy(vueltaStrings , "Patricio") ; 
vueltaString (vueltaStrings) ; 
free(vueltaStrings) ; 

/*--------------------------pregunta 9 -------------------------------------------*/ 
Biblioteca *PunteroLibro1 , Libro1 ; 
PunteroLibro1 = &Libro1 ; 
PunteroLibro1 ->nameBook = (char*)malloc(10*sizeof(char)) ; 
if ((PunteroLibro1 ->nameBook )== NULL){
    printf ("no se pudo asignar memmoria \n") ;
    return -1  ; 
}else {
    strcpy(PunteroLibro1 ->nameBook  , "One piece" ) ; 
    printf("nombre del libro es %s \n ",  PunteroLibro1 ->nameBook ) ; 
}
PunteroLibro1 ->nameAuthor = (char*)malloc(10*sizeof(char)) ; 
if ((PunteroLibro1 ->nameAuthor )== NULL){
    printf ("no se pudo asignar memmoria \n") ;
    return -1  ; 
}else {
    strcpy(PunteroLibro1 ->nameAuthor  , "E. Oda" ) ; 
    printf("nombre del Autor es %s \n ",  PunteroLibro1 ->nameAuthor ) ; 
}


return 0 ; 
}