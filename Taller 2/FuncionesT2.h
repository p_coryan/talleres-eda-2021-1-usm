#ifndef FUNCIONEST2_H 
#define FUNCIONEST2_H

#include <stdio.h>
#include <stdlib.h>

void NumeroMayor(int* , int* , int*);
void generarElementos(int *  , int * );
int factorial(int ) ;
void permutarCaracter(char *  , int* ) ;
int lengthString (char* ) ; 
void swap (int * , int * ) ;
void vueltaString (char* );

typedef struct  {
    char *nameBook ; 
    char *nameAuthor ; 
} Biblioteca ; 



#endif 